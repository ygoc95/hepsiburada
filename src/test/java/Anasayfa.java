import com.thoughtworks.gauge.Step;
import helper.ElementHelper;
import helper.StoreHelper;
import model.ElementInfo;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Anasayfa extends baseTest {

    String url = "https://hepsiburada.com";


    private WebElement findElement(String key){
        ElementInfo elementInfo = StoreHelper.INSTANCE.findElementInfoByKey(key);
        By infoParam = ElementHelper.getElementInfoToBy(elementInfo);
        WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        WebElement webElement = webDriverWait
                .until(ExpectedConditions.presenceOfElementLocated(infoParam));
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView({behavior: 'smooth', block: 'center', inline: 'center'})",
                webElement);
        return webElement;
    }
    //Sayfa yüklenmesini kontrol için title kontrolü
    @Step("<title> title kontrol et")
    public void titleKontrol(String title) {
        Assert.assertEquals(title, driver.getTitle());
        System.out.println("--------SAYFA YUKLENDI:"+title+"--------");
    }

    //Anasayfaya gidiş
    @Step({"Anasayfaya git"})
    public void anasayfayaGit() throws InterruptedException {
        driver.get(url);
        Thread.sleep(4000);
    }


    @Step("<elementKey> ustune gel ve ac")
    public void girisMenusunuAc( String elementKey) throws InterruptedException {
        action.moveToElement(findElement(elementKey)).click().build().perform();
        Thread.sleep(4000);
    }
    @Step("<input> ile <elementKey> bolumunu doldur")
    public void inputDoldur(String input,String elementKey) throws InterruptedException {
        findElement(elementKey).sendKeys(input);
        Thread.sleep(4000);
    }

    @Step("<elementKey> e tikla")
    public void tıkla(String elementKey) throws InterruptedException {
        findElement(elementKey).submit();
        Thread.sleep(4000);
    }

    @Step("<isim> ile karsilastir")
    public void isimKarsilastir(String isim) {
        String expected = driver.findElement(By.xpath("")).getText();
        Assert.assertEquals(isim,expected);
        System.out.println("--------ISIMLER AYNI--------");

    }
}


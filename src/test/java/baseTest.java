import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class baseTest {
    @BeforeScenario
    public void senaryoOncesi() {
        System.out.println("-----Senaryo başlangıcı----");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        action = new Actions(driver);
        driver.manage().window().maximize();
    }

    static WebDriver driver;
    static Actions action;



    @AfterScenario
    public void senaryoSonrasi() {
        driver.quit();
        System.out.println("-----Senaryo sonu----");
    }
}
